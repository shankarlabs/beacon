<h3>Beacon</h3>, <h5>current version <strong>0.2</strong></h5>

<br> <br>

Beacon is an open source library, aimed at reducing the effort to set up Google Cloud Messaging in an Android application.
Most of the core/common code is taken and built into a library that can then be just included and set up with just a few methods

<br> <br>

<h4><strong>Usage -</strong></h4>
First, you need to check if the device has Play Services installed, to use GCM.
    <ul>
        <li>Add <strong>Helper.checkPlayServices(Context, Activity)</strong> in
            <strong>onCreate()</strong> and <strong>onResume()</strong> of the app </li>
        <li>Pass the Application Context from getApplicationContext(), and the Activity this is being used in. </li>
        <li>Context is to check for Play Services, and Activity is to display Dialog if Play Services is not present </li>
    </ul>
<br> <br>

Next, set up the connection to external server that stores the GCM IDs. <br>
    These parameters are used for a connection as soon as we have a GCM ID. <br>
    Beacon always appends the registered GCM ID as 'GCMID' to 'postData'. You don't have to include that in the values when setting up
    <ul>
        <li>Call <strong>BeaconSettings.setupConnection(Url, connectionType, headers, postData)</strong> to set up everything </li>
        <li><strong>Url</strong> is of type String, it's a complete URL to which data will be POSTed </li>
        <li><strong>connectionType</strong> is int, either BeaconSettings.REST or BeaconSettings.HTTP depending on your server </li>
        <li><strong>headers and postData</strong> are <pre>ArrayList<NameValuePair></pre>, containing all header/data to be sent to the server </li>
    </ul>
<br> <br>

Next, call <strong>Beacon.registerAndSave()</strong>, which initializes, does a few checks, and completes the registration process, and you're done.
    <ul>
        <li>Beacon registers the app for a GCM ID </li>
        <li>As soon as it has a GCM ID, it saves it to the external server by calling saveToServer() with params set up in BeaconSettings </li>
        <li>A copy of the ID is also saved locally in the Preferences for your own use. </li>
    </ul>
<br> <br>

That's it ! You're done. Go back to your server and check for the newly arrived GCM ID !

<h4><strong>Some more information - </strong></h4>
<ul>
    <li>Beacon unregisters and re-registers a GCM ID on every app version change, and saves the same to the external server. </li>
    <li>The external server must be capable of detecting the change and updating the IDs as necessary. </li>
</ul>

<h4><strong>Known Issues or debugging : </strong></h4>
        <ul>
            <li>The library has a Crashlytics.jar in the libs directory, that could throw some exceptions when compiling
                if you're using the same library in your project. You can remove the jar from Beacon's libs directory </li>
            <li>If you're having problems with permissions running the application with the library included,
                make sure you include these permissions in your manifest file, though they're already included in the manifest -
                <br><code><uses-permission android:name="com.google.android.c2dm.permission.RECEIVE" /> </code>
                <br><code><uses-permission android:name="android.permission.READ_PHONE_STATE" /> </code>
            </li>
        </ul>