package com.shankarlabs.beacon;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.util.Set;

/**
 * BeaconIntentService is a Service that takes care of all the Intents received from using the GCM Service
 * and is responsible for handling them appropriately. This IntentService is supposed to be used only as a placeholder
 * It receives the intents and displays them all as a Notification.
 */
public class BeaconIntentService extends IntentService {
    private static final String LOGTAG = "Beacon:IntentService";
    private final int NOTIFICATION_ID = 300;

    public BeaconIntentService() {
        super("BeaconIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging cloudMessagingClient = GoogleCloudMessaging.getInstance(this);

        // The getMessageType() intent parameter must be the intent you received in your BroadcastReceiver.
        String messageType = cloudMessagingClient.getMessageType(intent);

        if (!extras.isEmpty()) {
            if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
                sendNotification("Send error : " + extras.toString());
            } else if (GoogleCloudMessaging.ERROR_MAIN_THREAD.equals(messageType)) {
                sendNotification("GCM Register/Unregister blocking all Communications. " + extras.toString());
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) { // Regular Message
                StringBuilder stringBuilder = new StringBuilder("");
                Object obj;
                Set<String> bundleKeys = extras.keySet();
                for(String key : bundleKeys) {
                    obj = extras.get(key);
                    stringBuilder.append(key + " : " + obj.getClass().cast(obj));
                }
                sendNotification(stringBuilder.toString());
            }
        }

        // Release the wake lock provided by the WakefulBroadcastReceiver.
        BeaconBroadcastReceiver.completeWakefulIntent(intent);
    }

    // Display a Notification
    private void sendNotification(String msg) {
        // Try and get the main class for the application
        Class mainClass = null;
        try {
            mainClass = this.getPackageManager().getPackageInfo(getPackageName(), 0).applicationInfo.getClass();
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        NotificationManager notificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, mainClass), 0);

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_stat_gcm)
                        .setContentTitle("Beacon : Server Message")
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
                        .setContentText(msg);

        notificationBuilder.setContentIntent(contentIntent);
        notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());
    }
}
