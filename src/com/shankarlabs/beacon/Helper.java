package com.shankarlabs.beacon;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

/**
 * Helper class is a set of static methods that are generally useful elsewhere and can be used as-is in other projects.
 */
public class Helper {

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 340;
    private static final String LOGTAG = "Beacon:Helper";

    public static boolean checkPlayServices(Context context, Activity activity) {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, activity,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.w(LOGTAG, "Play Services not found. This device is not supported.");
            }
            return false;
        }
        return true;
    }

    public static void testDisplayDialog(Context context) {
        new AlertDialog.Builder(context)
                .setTitle("Test Dialog created from a Library")
                .setMessage("Test Message to be displayed in the Test Dialog")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // DOn't need to change anything; Click the the button will dismiss it anyway
                    }
                })
                .setCancelable(true)
                .create()
                .show();
    }
}
