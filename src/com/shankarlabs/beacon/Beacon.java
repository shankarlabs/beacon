package com.shankarlabs.beacon;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

/**
 * Beacon is an Android Library Project that automates the process of setting up, initializing, and receiving
 * GCM Messages on Android devices
 * The only changes that need to be done are the signature updates in the AndroidManifest.xml
 *
 */
public class Beacon {
    private Context mContext;
    private static Beacon beacon;
    private static final String LOGTAG = "Beacon";
    private static String senderID;
    private String GCMRegistrationId;
    public GoogleCloudMessaging cloudMessagingClient;
    private int exponentialBackoffTimer = 15, savedAppVersionCode, currentAppVersionCode;
    private final int REGISTER_AGAIN = 30;
    private static SharedPreferences sharedPrefs;
    private static SharedPreferences.Editor sharedPrefsEditor;

    private Beacon(Context context, String senderId) {
        if(BeaconSettings.debugLogging())
            Log.d(LOGTAG, "Constructor : Setting up Beacon with context " + context.toString() + ". sender ID " + senderId);
        if(mContext == null) {
            mContext = context;
        }

        Crashlytics.start(mContext);

        senderID = senderId;
        sharedPrefs = mContext.getSharedPreferences(mContext.getPackageName(),
                Context.MODE_PRIVATE);
        sharedPrefsEditor = sharedPrefs.edit();

        checkAndRegister(mContext);
    }

    public static void registerAndSave(Context context, String senderId) {
        if(beacon == null) {
            if(BeaconSettings.debugLogging())
                Log.d(LOGTAG, "setup : Setting up new Beacon with context " + context.toString() + ", sender ID " + senderId);
            beacon = new Beacon(context, senderId);
        }
    }

    private void checkAndRegister(Context context) {
        try {
            currentAppVersionCode = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0).versionCode;
            savedAppVersionCode = sharedPrefs.getInt("AppVersionCode", 0);
            if(BeaconSettings.debugLogging())
                Log.d(LOGTAG, "checkAndRegister : Current App Version : " + currentAppVersionCode + ", saved App Version " + savedAppVersionCode);

            // if(getSavedGCMRegistrationId().isEmpty()) { // We're not registered for GCM. Register now.
            if(true) { // Only for testing purposes, should be removed soon
                if(BeaconSettings.debugLogging())
                    Log.d(LOGTAG, "checkAndRegister : Registering for GCM ID in background");
                registerInBackground();
            } else if(currentAppVersionCode != savedAppVersionCode) { // If there's a versionCode mismatch, register again
                if(BeaconSettings.debugLogging())
                    Log.d(LOGTAG, "checkAndRegister : Unregistering and registering again for GCM ID in background");
                registerAgainInBackground();
            } else {
                Log.d(LOGTAG, "Already registered for GCM. Moving on.");
            }
        } catch (PackageManager.NameNotFoundException nnfe) {
            Log.e(LOGTAG, "Package Name not found, " + nnfe.getMessage());
            nnfe.printStackTrace();
        }

    }

    /**
     * Checks the preferences for any previously saved GCM ID that was registered and returns it
     */
    private String getSavedGCMRegistrationId() {
        if(BeaconSettings.debugLogging())
            Log.d(LOGTAG, "getSavedGCMRegistrationId : GCM ID saved on device : " + sharedPrefs.getString("GCMRegistrationId", ""));
        return sharedPrefs.getString("GCMRegistrationId", "");
    }

    /**
     * Saves the Registration ID passed to it as a SharedPreference for future use
     * Also saves the current App Version Code value every time the GCM ID is saved
     * @param registraionID GCM Registration ID to be saved
     */
    private void saveGCMRegistrationId(String registraionID) {
        if(BeaconSettings.debugLogging())
            Log.d(LOGTAG, "saveGCMRegistrationId : Saving GCM ID to device : " + registraionID + ", App version " + currentAppVersionCode);
        sharedPrefsEditor.putString("GCMRegistrationId", registraionID);
        sharedPrefsEditor.putInt("AppVersionCode", currentAppVersionCode);
        sharedPrefsEditor.commit();
    }

    /**
     * Checks the Shared Preferences for a saved Device ID and returns it
     * @return Returns the Device ID saved in the Preferences
     */
    private static String getSavedDeviceId() {
        if(BeaconSettings.debugLogging())
            Log.d(LOGTAG, "getSavedDeviceId : Device ID saved on device : " + sharedPrefs.getString("DeviceId", ""));
        return sharedPrefs.getString("DeviceId", "");
    }

    /**
     * Saves a Device ID in the Shared Preferences
     * @param deviceId The Device ID to be saved
     */
    private static void saveDeviceId(String deviceId) {
        if(BeaconSettings.debugLogging())
            Log.d(LOGTAG, "saveDeviceId : Saving Device ID to device : " + deviceId);
        sharedPrefsEditor.putString("DeviceId", deviceId);
        sharedPrefsEditor.commit();
    }

    /**
     * Starts the Background Task to try and register for the GCM ID
     */
    private void registerInBackground() {
        backgroundRegistrationAsyncTask.execute(null, null, null);
    }

    /**
     * Starts the Background Task to try and unregister for the GCM ID
     */
    private void unregisterInBackground() {
        backgroundUnregisterAsyncTask.execute(null, null, null);
    }

    /**
     * Starts the Background Task to try and register again for the GCM ID
     */
    private void registerAgainInBackground() {
        backgroundUnregisterAsyncTask.execute(REGISTER_AGAIN, null, null);
    }

    /**
     * Implpements exponential backoff in it's simplest form with growing sleeps
     */
    private void registerWithExponentialBackoff() {
        new Runnable() {
            @Override
            public void run() {
                try {
                    if(BeaconSettings.debugLogging())
                        Log.d(LOGTAG, "registerWithExponentialBackoff : Sleeping for " + exponentialBackoffTimer + " seconds for exponential Backoff");
                    Thread.sleep((exponentialBackoffTimer * 1000));
                    backgroundRegistrationAsyncTask.execute(null, null, null);
                } catch (InterruptedException ie) {
                    Log.e(LOGTAG, "Exception waiting for exponential Backoff, " + ie.getMessage());
                    ie.printStackTrace();
                }
            }
        }.run();
    }

    /**
     * Saves the GCM ID to a third party server, with parameters set in BeaconSettings
     * Performs the HTTP calls on a separate thread, not blocking the UI thread
     */
    private static void saveToServer() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                switch(BeaconSettings.getConnectionType()) {
                    case BeaconSettings.REST:
                        if(BeaconSettings.debugLogging())
                            Log.d(LOGTAG, "saveToServer : Saving all data to external server via REST");
                        new GcmRestClient().execute();
                        break;
                    case BeaconSettings.HTTP:
                        if(BeaconSettings.debugLogging())
                            Log.d(LOGTAG, "saveToServer : Saving all data to external server via HTTP");
                        new GcmHttpClient().execute();
                        break;
                    default:
                        Log.d(LOGTAG, "Unknown Save request. Nothing to do");
                }
                return null;
            }
        }.execute(null, null, null);
    }

    /**
     * Gets a unique device ID for the device and hashes it making it safe to use in public.
     * Stolen off of Koushik Dutta's post on Google Plus
     * @param context
     * @return
     */
    public static String getDeviceId(Context context) {
        String usableDigest = null;
        String savedDeviceId = getSavedDeviceId();
        if(!savedDeviceId.isEmpty()) { // We have a device ID saved on the device already
            return savedDeviceId;
        }

        // Get the device ID of the device if there's telephony
        TelephonyManager tm = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
        String deviceId = tm.getDeviceId();
        if(BeaconSettings.debugLogging())
            Log.d(LOGTAG, "getDeviceId : Device ID obtained from Telephony Service : " + deviceId);

        // Check if we have an ID; Try getting WiFI MAC if we don't
        if (deviceId == null) {
            WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            if(wifiManager.isWifiEnabled()) { // We get the MAC only if WiFi is enabled. Else it'd fail anyway
                deviceId = wifiManager.getConnectionInfo().getMacAddress();
            }
        }
        if(BeaconSettings.debugLogging())
            Log.d(LOGTAG, "getDeviceId : Device ID obtained from WiFi Service : " + deviceId);

        // Check for deviceID again, and use 000000000000 if we still don't have an ID
        if (deviceId == null) {
            deviceId = getRandomString(10);
            Log.d(LOGTAG, "No Device ID found, using " + deviceId);
        }

        // Now compute a one-way hash
        try {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            md5.update((deviceId + context.getPackageName()).getBytes());
            byte[] tempDigest = md5.digest();
            StringBuilder sb = new StringBuilder();
            for(int i = 0; i < tempDigest.length; i++) // Generate teh Hex Digest
                sb.append(Integer.toHexString(0xFF & tempDigest[i]));

            usableDigest = sb.toString();
        } catch (NoSuchAlgorithmException e) {
            Log.e(LOGTAG, e.getMessage());
            e.printStackTrace();
        }

        if(BeaconSettings.debugLogging())
            Log.d(LOGTAG, "getDeviceId : Hashed Device ID ready for use : " + usableDigest);
        saveDeviceId(usableDigest);
        return usableDigest;
    }

    AsyncTask<Void, Void, Void> backgroundRegistrationAsyncTask = new AsyncTask<Void, Void, Void>() {
        @Override
        protected Void doInBackground(Void... voids) {
            try {
                if(BeaconSettings.debugLogging())
                    Log.d(LOGTAG, "RegistrationAsyncTask : Starting to register for GCM");

                if(cloudMessagingClient == null) {
                    if(BeaconSettings.debugLogging())
                        Log.d(LOGTAG, "RegistrationAsyncTask : cloudMessagingClient is null, getting one with context " + mContext.toString());
                    cloudMessagingClient = GoogleCloudMessaging.getInstance(mContext);
                }

                if(BeaconSettings.debugLogging())
                    Log.d(LOGTAG, "RegistrationAsyncTask : Obtained cloudMessagingClient " + cloudMessagingClient.toString() + ", Registering for GCM now with sender ID " + senderID);
                GCMRegistrationId = cloudMessagingClient.register(senderID);
                if(GCMRegistrationId.isEmpty() || GCMRegistrationId == null) {
                    exponentialBackoffTimer *= 2; // Start with 30 seconds, and then double on each failure
                    if(exponentialBackoffTimer < 1950) { // Try at 0.5, 1, 2, 4, 8, 16, 32 minute intervals.
                        if(BeaconSettings.debugLogging())
                            Log.d(LOGTAG, "RegistrationAsyncTask : Received GCMRegistration ID from Google : " + GCMRegistrationId +
                                    ", waiting " + exponentialBackoffTimer + " seconds before trying again");
                        registerWithExponentialBackoff();
                        return null;
                    } else {
                        Log.w(LOGTAG, "Failed to register for GCM. Please try again later");
                        saveGCMRegistrationId("");
                        return null;
                    }
                } else {
                    if(BeaconSettings.debugLogging())
                        Log.d(LOGTAG, "RegistrationAsyncTask : Received GCMRegistration ID from Google : " + GCMRegistrationId + ", saving to device and URL " + BeaconSettings.getUrl());
                    saveGCMRegistrationId(GCMRegistrationId);
                    BeaconSettings.addPostData("GCMID", GCMRegistrationId);
                    saveToServer();

                    // The ID should already be saved to Prefs.
                    // Setting up external server params and saving ID to a server should be done by user
                    // sendGCMIdToServer(GCMRegistrationId);
                    return null;
                }
            } catch(IOException ioe) {
                Log.e(LOGTAG, "Exception trying to register for Cloud Messaging, " + ioe.getMessage());
                ioe.printStackTrace();
                return null;
            }
        }
    };

    AsyncTask<Integer, Integer, Integer> backgroundUnregisterAsyncTask = new AsyncTask<Integer, Integer, Integer>() {
        @Override
        protected Integer doInBackground(Integer... unregisterType) {
            try {
                if(cloudMessagingClient == null) {
                    cloudMessagingClient = GoogleCloudMessaging.getInstance(mContext);
                }
                cloudMessagingClient.unregister();

                if (unregisterType[0] == REGISTER_AGAIN) {
                    // Register again, and that'll take care of saving the ID locally and remmotely
                    backgroundRegistrationAsyncTask.execute(null, null, null);
                } else {
                    // Update the GCM ID locally and remotely
                    sharedPrefsEditor.putInt("AppVersionCode", 0);
                    sharedPrefsEditor.commit();
                    BeaconSettings.addPostData("GCMID", "");
                    saveToServer();
                }
                return null;
            } catch(IOException ioe) {
                Log.e(LOGTAG, "Exception trying to unregister from Cloud Messaging, " + ioe.getMessage());
                ioe.printStackTrace();
                return null;
            }
        }
    };

    private static String getRandomString(int length) {
        final char[] symbols = new char[36]; // For generating a random String
        final Random random = new Random();

        if (length < 1)
            length = 10; // Default length of the random String

        // Create the alphanumeric block for random string
        for (int idx = 0; idx < 10; ++idx)
            symbols[idx] = (char) ('0' + idx);
        for (int idx = 10; idx < 36; ++idx)
            symbols[idx] = (char) ('a' + idx - 10);

        final char[] buf = new char[length];
        for (int idx = 0; idx < buf.length; ++idx)
            buf[idx] = symbols[random.nextInt(symbols.length)];
        return new String(buf);
    }
}
