package com.shankarlabs.beacon;

import android.util.Log;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 * Posts a given set of data to a given server as a normal HTTP call.
 * All Headers and data to be POSTed to the server needs to be provided by the user through BeaconSettings
 * before executing the GcmHttpClient. GcmHttpClient just takes all values from BeaconSettings and POSTs it to the URL.
 */
public class GcmHttpClient {
    private static final String LOGTAG = "Beacon:GcmHttpClient";

    private void setHeaders(HttpUriRequest request) {
        for(NameValuePair header : BeaconSettings.getHeaders()) {
            if(BeaconSettings.debugLogging())
                Log.d(LOGTAG, "setHeaders : Adding Header, name : " + header.getName() + ", value : " + header.getValue());
            request.addHeader(header.getName(), header.getValue());
        }
    }

    public boolean execute() {

        HttpClient httpClient = new DefaultHttpClient();
        HttpPost request = new HttpPost(BeaconSettings.getUrl());
        setHeaders(request);

        try {
            ArrayList<NameValuePair> postData = BeaconSettings.getPostData();
            if(BeaconSettings.debugLogging()) {
                for(NameValuePair data : postData) {
                    Log.d(LOGTAG, "execute : Adding Data to request, name : " + data.getName() + ", value : " + data.getValue());
                }
            }
            StringEntity entity = new UrlEncodedFormEntity(postData);
            entity.setContentType("text/html");
            request.setEntity(entity);

            HttpResponse httpResponse = httpClient.execute(request);
            Integer responseCode = httpResponse.getStatusLine().getStatusCode();
            String responseMessage = httpResponse.getStatusLine().getReasonPhrase();
            byte[] responseBuffer = new byte[1024];
            httpResponse.getEntity().getContent().read(responseBuffer);

            Log.d(LOGTAG, "Response code: " + responseCode);
            Log.d(LOGTAG, "Response message: " + responseMessage);
            Log.d(LOGTAG, "Server Response : " + new String(responseBuffer));

            return responseCode == HttpStatus.SC_OK;

            // externalServerSaveStateFlag;

        } catch( UnsupportedEncodingException uee ) {
            Log.e( LOGTAG, "UnsupportedEncodingException: " + uee.getMessage() );
            uee.printStackTrace();
            return false;
        } catch (ClientProtocolException cpe)  {
            httpClient.getConnectionManager().shutdown();
            Log.e(LOGTAG, "ClientProtocolException: " + cpe.getMessage());
            cpe.printStackTrace();
        } catch (IOException ioe) {
            httpClient.getConnectionManager().shutdown();
            Log.e(LOGTAG, "IOException: " + ioe.getMessage());
            ioe.printStackTrace();
        } catch (Exception e) {
            Log.e(LOGTAG, "Exception: " + e.getMessage());
            e.printStackTrace();
        }

        return false;
    }
}
