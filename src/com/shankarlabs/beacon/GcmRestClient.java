package com.shankarlabs.beacon;

import android.util.Log;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * Posts a given set of data to a given server as REST call.
 * All Headers and data to be POSTed to the server needs to be provided by the user through BeaconSettings
 * before executing the GcmRestClient. GcmRestClient just takes all values from BeaconSettings and POSTs it to the URL.
 * GcmRestClient.java implementation obtained from Adam Bogdal's Pager, a GCM Client implementation.
 * Available on github at "https://github.com/bogdal/pager"
 */
public class GcmRestClient {
    private static final String LOGTAG = "Beacon:RestClient";

    private void setHeaders(HttpUriRequest request) {
        for(NameValuePair header : BeaconSettings.getHeaders()) {
            request.addHeader(header.getName(), header.getValue());
        }
    }

    public JSONObject getJsonObject() {
        JSONObject jsonObj = new JSONObject();

        for(NameValuePair data : BeaconSettings.getPostData()) {
            try {
                jsonObj.put(data.getName(), data.getValue());
            } catch (JSONException e) {
                Log.e(LOGTAG, "JSONException: " + e.getMessage());
                e.printStackTrace();
            }
        }
        return jsonObj;
    }

    public boolean execute() {
        HttpPost request = new HttpPost(BeaconSettings.getUrl());
        setHeaders(request);

        try {
            StringEntity entity = new StringEntity(getJsonObject().toString(), HTTP.UTF_8);
            entity.setContentType("application/json");
            request.setEntity(entity);
        } catch( UnsupportedEncodingException uee ) {
            Log.e( LOGTAG, "UnsupportedEncodingException: " + uee.getMessage() );
            uee.printStackTrace();
            return false;
        }

        HttpClient client = new DefaultHttpClient();
        try {

            HttpResponse httpResponse = client.execute(request);
            Integer responseCode = httpResponse.getStatusLine().getStatusCode();
            String responseMessage = httpResponse.getStatusLine().getReasonPhrase();

            Log.d(LOGTAG, "Response code: " + responseCode);
            Log.d(LOGTAG, "Response message: " + responseMessage);

            return responseCode == HttpStatus.SC_OK;

        } catch (ClientProtocolException cpe)  {
            client.getConnectionManager().shutdown();
            Log.e(LOGTAG, "ClientProtocolException: " + cpe.getMessage());
            cpe.printStackTrace();
        } catch (IOException ioe) {
            client.getConnectionManager().shutdown();
            Log.e(LOGTAG, "IOException: " + ioe.getMessage());
            ioe.printStackTrace();
        } catch (Exception e) {
            Log.e(LOGTAG, "Exception: " + e.getMessage());
            e.printStackTrace();
        }

        return false;
    }
}
