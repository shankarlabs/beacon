package com.shankarlabs.beacon;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;

/**
 * The class that holds all of the settings and variables needed to communicate with third party servers
 * All properties need to be updated through their setter methods, or an update method updates them all at a time.
 */
public class BeaconSettings {
    private static String url = ""; // URL of the server to talk to
    private static ArrayList<NameValuePair> headers; // Headers for the connection
    private static ArrayList<NameValuePair> postData; // Data to be sent to the server as POST
    public static final int HTTP = 20, REST = 30;
    private static int connectionType = HTTP; // Default connection type to HTTP
    private static boolean debugLogging = false; // No debug logging be default

    /**
     * The static method to get/update all parameters required to make a call to the third party server
     * @param newUrl URL of the third party server. All headers and data will be sent through POST
     * @param newConnectionType Connection type of the third party server. HTTP or Rest. Rest sends out all data REST style, HTTP sends out HTTP style
     * @param newHeaders Custom headers if any need to be passed out, null if no custom headers. Content-Type is a default header sent out when null is received
     * @param newPostData NameValuePairs of all the Data that needs to be sent to the servers.
     */
    public static void setupConnection(String newUrl,
                              int newConnectionType,
                              ArrayList<NameValuePair> newHeaders,
                              ArrayList<NameValuePair> newPostData) {
        url = newUrl;
        connectionType = newConnectionType;
        if(headers == null) {
            headers = new ArrayList<NameValuePair>();
            headers.add(new BasicNameValuePair("Content-Type", "application/x-www-form-urlencoded")); // The only header we need to send out be default
            headers.add(new BasicNameValuePair("User-Agent", "Teleport/UserAgent")); // Custom User Agent
            // headers.add(new BasicNameValuePair("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) Chrome/6.0.472.55 Safari/534.3"));
        } else {
            headers = newHeaders;
        }
        postData = newPostData;
    }

    public static void setUrl(String newUrl) {
        url = newUrl;
    }

    public static String getUrl() {
        return url;
    }

    public static void setDebugMode(boolean debugMode) {
        debugLogging = debugMode;
    }

    public static boolean debugLogging() {
        return debugLogging;
    }

    public static void setHeaders(ArrayList<NameValuePair> newHeaders) {
        headers = newHeaders;
    }

    public static ArrayList<NameValuePair> getHeaders() {
        return headers;
    }

    public static void addPostData(String key, String value) {
        NameValuePair nvp = new BasicNameValuePair(key, value);
        postData.add(postData.size(), nvp);
    }

    public static void setPostData(ArrayList<NameValuePair> newPostData) {
        postData = newPostData;
    }

    public static ArrayList<NameValuePair> getPostData() {
        return postData;
    }

    public static void setConnectionType(int newConnectionType) {
        connectionType = newConnectionType;
    }

    public static int getConnectionType() {
        return connectionType;
    }
}



