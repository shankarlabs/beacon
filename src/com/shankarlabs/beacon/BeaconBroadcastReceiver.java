package com.shankarlabs.beacon;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

/**
 * BroadcastReceiver that receives the GCM Broadcasts from the GCM Server
 * and hands it over to BeaconIntentService where the rest of the processing takes place
 * This class is to just hold the Wake Lock while the app receives the GCM Message
 */
public class BeaconBroadcastReceiver extends WakefulBroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        // Explicitly specify that GcmIntentService will handle the intent.
        ComponentName component = new ComponentName(context.getPackageName(),
                BeaconIntentService.class.getName());
        // Start the service, keeping the device awake while it is launching.
        startWakefulService(context, (intent.setComponent(component)));
        setResultCode(Activity.RESULT_OK);
    }
}
